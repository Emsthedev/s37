//EXPRESS SERVER SETUP
const express = require('express');
// Mongoose is a package that allows the ceation of schemas to model
//our data structures
const mongoose = require('mongoose');
//cors dependecies- allws backend app to be available for use in front end app.
//allows us to control the app'sCross-Origin Resources Sharing Settings
const cors = require('cors');

// const taskRoutes = require('./routes/taskRoutes.js')

const app = express();
const port = 4000;

//middleware
//cors allows all resources to access our back end app
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//establishing connection mongodb
//[Section] - Mongoose Connection
//Mongoose uses the 'connect ' func to connect to the cluster in our MondoDB Atlas
/* 
It takes 2 arguments:
1.connection string from our mondodb atlas
2.obect that conatins the middlewares/standars that Mondo DB uses
*/

mongoose.connect(`mongodb+srv://root:root@zuitt-b197pt.xjoh3wx.mongodb.net/S37-s41-discussion?retryWrites=true&w=majority`,{
   //it allows us to avoid any 
   //current and/or future error while connecting to Mongo DB
    useNewUrlParser : true,
    //if false :unifiedtopology by default, we need to set it to true to opt in
    //to using the MongoDB driver's new connection managment engine.
    useUnifiedTopology : true
})
//initializes the mongoose connection to the MongoDB by 
//assigning mongoose.connection to the 'db' variable
let db = mongoose.connection

/* 
Listener events of the connection by using the .on() function
of the mongoose connection and logs the details in 
the console based of the event (error or successful)
 */
db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'));

//middleware
//http://localhost:3001/tasks/(/URI)
// app.use('/tasks', taskRoutes)



app.listen(port, () => console.log(`API is now online at port ${port}`))












 




